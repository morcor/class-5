// Attach one listener that will detect clicks on any of the <td>
// elements.  Should update that element's innerHTML to be the
// x, y coordinates of the mouse at the time of the click


let clickableGrid = document.getElementsByTagName('tbody')[0];
function getClickPosition(xy) {
    console.log(`${xy.clientX}, ${xy.clientY}`);
};
clickableGrid.addEventListener('click', (xy) => {
    getClickPosition(xy);
    position = xy.target;
    position.innerHTML = `${xy.clientX}, ${xy.clientY}`;
 });