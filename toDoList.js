// If an li element is clicked, toggle the class "done" on the <li>

const deleteElement = '<a class="delete">Delete</a>';
const ul = document.getElementsByClassName("today-list")[0]

  document.body.addEventListener('click', function(event) {
    const clickTarget = (event.target)  
    const listItem = clickTarget.closest("li")
    if (clickTarget.className === "delete") {
      listItem.remove()
    } else {
      listItem.classList = 'done';
    }
  });


// If a delete link is clicked, delete the li element / remove from the DOM
// If an 'Add' link is clicked, adds the item as a new list item with
// addListItem function has been started to help you get going!

const addLink = document.querySelector("a.add-item");
addLink.addEventListener("click", addListItem);

// Make sure to add an event listener(s) to your new <li> (if needed)

function addListItem(e) {
  e.preventDefault();
  const input = this.parentNode.getElementsByTagName('input')[0];
  const text = input.value; // use this text to create a new <li>
  // Finish function here
  const newListElement = document.createElement("li")
  const newTextContent = document.createElement("span")

  newTextContent.innerText = text
  newListElement.appendChild(newTextContent)
  newListElement.insertAdjacentHTML("beforeend", deleteElement)
  console.log(newListElement)

  ul.appendChild(newListElement)
};
