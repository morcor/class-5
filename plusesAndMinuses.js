// When a user clicks the + element, the count should increase by 1 on screen.


let currentCount = 0;

const handleCount = (e) => {
    e.preventDefault();
    currentCount = e.type === "click" ? currentCount + 1 : currentCount - 1;
    document.getElementById("count2").innerHTML = currentCount;
};

let countBtn = document.getElementById("evtType");
countBtn.addEventListener("click", handleCount);
// countBtn.addEventListener("contextmenu", handleCount);


// When a user clicks the – element, the count should decrease by 1 on screen.

const handleNegCount = (e) => {
    e.preventDefault();
    currentCount = e.type === "click" ? currentCount - 1 : currentCount + 1;
    document.getElementById("count2").innerHTML = currentCount;
};

const countBtnTwo = document.getElementById("negEvtType");
countBtnTwo.addEventListener("click", handleNegCount);