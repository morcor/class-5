// Do not change
document.getElementById('cat').addEventListener('click', () => {
  alert('meow!');
  preventDefault();
});

// When clicked, "More info" link should alert "Here's some info"
// instead of going to a new webpage

document.getElementById("more-info").addEventListener("click", function(event){
  event.preventDefault()
});

document.getElementById("more-info").addEventListener('click', () => {
  alert("Here's some info");
});


// When the bark button is clicked, should alert "Bow wow!"
// Should *not* alert "meow"


document.getElementById('cat').removeEventListener('click', function(){
  
});


document.getElementById("dog").addEventListener('click', () => {
  alert("Bow wow!");
});
